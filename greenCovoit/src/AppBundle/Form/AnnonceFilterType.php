<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;



use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class AnnonceFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('villedepart', TextType::class, array('required' => false))
            ->add('villearrivee', TextType::class, array('required' => false))
            ->add('datedepart', Filters\DateTimeRangeFilterType::class,
                                array('left_datetime_options' => array('widget' => 'single_text',
                                                                       'format' => 'dd/MM/yyyy HH:mm',
                                                                       'html5' => false,
                                                                       'attr' => array('class' => 'datepicker'),
                                                                       'data' => new \DateTime("now")
                                                                       ),
                                      'right_datetime_options' => array('widget' => 'single_text',
                                                                        'format' => 'dd/MM/yyyy HH:mm',
                                                                        'html5' => false,
                                                                        'attr' => array('class' => 'datepicker'),
                                                                        'data' => new \DateTime(date("Y-m-d H:00", strtotime("now +2 Hour")))
                                                                       ),
                                      )
                  )
                  ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
