<?php
// src/AppBundle/DataFixtures/ORM/LoadUser.php

namespace OC\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUser extends AbstractFixture implements OrderedFixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    $user = new User();
    $user->setname("moi");
    $user->setusername("moi");
    $user->setPlainPassword("moi");
    $user->setemail("moi@moi.fr");
    $user->setTel("0600000000");
    $user->setFirstname("moi");
    $user->setEnabled(true);
    $manager->persist($user);
    $this->addReference("moi", $user);

    $manager->flush();
  }

  public function getOrder() {
    return 1;
  }
}
