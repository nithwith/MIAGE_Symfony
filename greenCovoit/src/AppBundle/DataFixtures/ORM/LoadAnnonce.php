<?php
// src/AppBundle/DataFixtures/ORM/LoadAnnonce.php

namespace OC\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Annonce;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    $annonce = new Annonce();
    $annonce->setDriver($this->getReference("moi"));
    $annonce->setPrix(8);
    $annonce->setNbplacedispo(3);
    $annonce->setVilledepart("Nantes, France");
    $annonce->setPlaceDepart("ChIJra6o8IHuBUgRMO0NHlI3DQQ");
    $annonce->setPlaceArrivee("ChIJhZDWpy_eDkgRMKvkNs2lDAQ");
    $annonce->setVillearrivee("Rennes, France");
    $annonce->setDatedepart(new \DateTime("2017-03-20 23:30:00"));

    $manager->persist($annonce);

    $manager->flush();
  }

  public function getOrder() {
    return 2;
  }
}
