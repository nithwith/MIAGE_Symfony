<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $filterForm = $this->createForm('AppBundle\Form\AccueilAnnonceFilterType');
        return $this->render('default/homepage.html.twig', array(
            'filterForm' => $filterForm->createView(),

        ));
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction()
    {
        return $this->render('default/test.html.twig');
    }

    /**
     * Change the locale for the current user
     *
     * @param String $language
     * @return array
     *
     * @Route("/setlocale/{language}", name="setlocale")
     */
    public function setLocaleAction($language = null)
    {
            $this->get('session')->set('_locale', $language);

        // on tente de rediriger vers la page d'origine
        $url = $this->container->get('request')->headers->get('referer');
        if(empty($url))
        {
            $url = $this->container->get('router')->generate('index');
        }

        return new RedirectResponse($url);
    }
}
