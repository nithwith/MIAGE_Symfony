<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Eko\FeedBundle\Item\Writer\RoutedItemInterface;


/**
 * Annonce
 *
 * @ORM\Table(name="annonce")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnnonceRepository")
 */
class Annonce implements RoutedItemInterface
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="adsDriver")
     * @ORM\JoinColumn(nullable=false)
     */
    private $driver;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="adsTraveller")
     */
    private $travellers;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="prix", type="integer")
     */
    private $prix=0;

    /**
    * @ORM\Column(name="nbplacedispo", type="integer")
     * @var integer
     */
    private $nbplacedispo=0;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @var string
     */
    private $titre = "default";

    /**
     * @ORM\Column(type="string", length=32)
     * @var string
     */
    private $placeDepart="" ;

    /**
     * @ORM\Column(type="string", length=32)
     * @var string
     */
    private $placeArrivee="" ;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $descriptif;

    /**
     * @ORM\Column(type="string", length=32, nullable=false)
     * @var string
     */
    private $villedepart="" ;

    /**
     * @ORM\Column(type="string", length=32, nullable=false)
     * @var string
     */
    private $villearrivee="";

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var \DateTime
     */
    private $datedepart ;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datearrivee;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set prix
     *
     * @param integer $prix
     * @return Annonce
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nbplacedispo
     *
     * @param integer $nbplacedispo
     * @return Annonce
     */
    public function setNbplacedispo($nbplacedispo)
    {
        $this->nbplacedispo = $nbplacedispo;

        return $this;
    }

    /**
     * Get nbplacedispo
     *
     * @return integer
     */
    public function getNbplacedispo()
    {
        return $this->nbplacedispo;
    }

    /**
     * Set placeDepart
     *
     * @param string $placeDepart
     * @return Annonce
     */
    public function setPlaceDepart($placeDepart)
    {
        $this->placeDepart = $placeDepart;

        return $this;
    }

    /**
     * Get placeDepart
     *
     * @return string
     */
    public function getPlaceDepart()
    {
        return $this->placeDepart;
    }

    /**
     * Set placeArrivee
     *
     * @param string $placeArrivee
     * @return Annonce
     */
    public function setPlaceArrivee($placeArrivee)
    {
        $this->placeArrivee = $placeArrivee;

        return $this;
    }

    /**
     * Get placeArrivee
     *
     * @return string
     */
    public function getPlaceArrivee()
    {
        return $this->placeArrivee;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Annonce
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set descriptif
     *
     * @param string $descriptif
     * @return Annonce
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;

        return $this;
    }

    /**
     * Get descriptif
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Set villedepart
     *
     * @param string $villedepart
     * @return Annonce
     */
    public function setVilledepart($villedepart)
    {
        $this->villedepart = $villedepart;

        return $this;
    }

    /**
     * Get villedepart
     *
     * @return string
     */
    public function getVilledepart()
    {
        return $this->villedepart;
    }

    /**
     * Set villearrivee
     *
     * @param string $villearrivee
     * @return Annonce
     */
    public function setVillearrivee($villearrivee)
    {
        $this->villearrivee = $villearrivee;

        return $this;
    }

    /**
     * Get villearrivee
     *
     * @return string
     */
    public function getVillearrivee()
    {
        return $this->villearrivee;
    }

    /**
     * Set datedepart
     *
     * @param \DateTime $datedepart
     * @return Annonce
     */
    public function setDatedepart($datedepart)
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    /**
     * Get datedepart
     *
     * @return \DateTime
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }

    /**
     * Set datearrivee
     *
     * @param \DateTime $datearrivee
     * @return Annonce
     */
    public function setDatearrivee($datearrivee)
    {
        $this->datearrivee = $datearrivee;

        return $this;
    }

    /**
     * Get datearrivee
     *
     * @return \DateTime
     */
    public function getDatearrivee()
    {
        return $this->datearrivee;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->travellers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->datedepart = new \DateTime();
    }

    /**
     * Set driver
     *
     * @param \AppBundle\Entity\User $driver
     * @return Annonce
     */
    public function setDriver(\AppBundle\Entity\User $driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \AppBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Add travellers
     *
     * @param \AppBundle\Entity\User $travellers
     * @return Annonce
     */
    public function addTraveller(\AppBundle\Entity\User $travellers)
    {
        if($this->nbplacedispo > 0) {
          $this->travellers[] = $travellers;
          $this->nbplacedispo--;
        }

        return $this;
    }

    /**
     * Remove travellers
     *
     * @param \AppBundle\Entity\User $travellers
     */
    public function removeTraveller(\AppBundle\Entity\User $travellers)
    {
        $this->travellers->removeElement($travellers);
        $this->nbplacedispo++;
    }

    /**
     * Get travellers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTravellers()
    {
        return $this->travellers;
    }


    /**
     * Returns annonce route name
     *
     * @return string
     */
    public function getFeedItemRouteName()
    {
        return 'annonce_show';
    }

    /**
     * Returns annonce route parameters
     *
     * @return array
     */
    public function getFeedItemRouteParameters()
    {
        return array('id' => $this->id);
    }

    public function getFeedItemTitle()
    {
        return array('id' => $this->id);
    }
    public function getFeedItemDescription()
    {
        return array('id' => $this->id);
    }
    public function getFeedItemUrlAnchor()
    {
        return array('id' => $this->id);
    }
    public function getFeedLink()
    {
        return array('id' => $this->id);
    }
    public function getFeedItemPubDate()
    {
        return array('id' => $this->id);
    }

}
