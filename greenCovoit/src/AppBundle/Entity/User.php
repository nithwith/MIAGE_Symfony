<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Annonce", mappedBy="driver")
     */
    private $adsDriver;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Annonce", mappedBy="travellers")
     */
    private $adsTraveller;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * )
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * )
     */
    protected $tel;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * )
     */
    protected $firstname;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname
     *
     * @param string firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    
    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Add adsDriver
     *
     * @param \AppBundle\Entity\Annonce $adsDriver
     * @return User
     */
    public function addAdsDriver(\AppBundle\Entity\Annonce $adsDriver)
    {
        $this->adsDriver[] = $adsDriver;

        return $this;
    }

    /**
     * Remove adsDriver
     *
     * @param \AppBundle\Entity\Annonce $adsDriver
     */
    public function removeAdsDriver(\AppBundle\Entity\Annonce $adsDriver)
    {
        $this->adsDriver->removeElement($adsDriver);
    }

    /**
     * Get adsDriver
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdsDriver()
    {
        return $this->adsDriver;
    }

    /**
     * Add adsTraveller
     *
     * @param \AppBundle\Entity\Annonce $adsTraveller
     * @return User
     */
    public function addAdsTraveller(\AppBundle\Entity\Annonce $adsTraveller)
    {
        $this->adsTraveller[] = $adsTraveller;

        return $this;
    }

    /**
     * Remove adsTraveller
     *
     * @param \AppBundle\Entity\Annonce $adsTraveller
     */
    public function removeAdsTraveller(\AppBundle\Entity\Annonce $adsTraveller)
    {
        $this->adsTraveller->removeElement($adsTraveller);
    }

    /**
     * Get adsTraveller
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdsTraveller()
    {
        return $this->adsTraveller;
    }
}
