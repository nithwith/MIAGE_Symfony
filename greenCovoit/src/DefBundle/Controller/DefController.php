<?php

namespace DefBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

/**
 * Annonce controller.
 *
 * @Route("/")
 */
class DefController extends Controller
{
  /**
   * Renvoi vers la page d'accueil
   *
   * @Route("/", name="def")
   */
  public function indexAction(Request $request) {
    return $this->redirectToRoute('homepage');
  }

}
